from PyQt4 import QtGui, QtCore, QtWebKit, uic
import requestfilters, networkcookiejar, history
from settings import *
from webpage import WebPage

class Tab(QtCore.QObject):
    settingsPrefix = "tabs"
    settingsDef = [
        BoolVar("tabsOpenRelative", True, name="New tabs open next to the current tab"),
        EnumVar("textElideMode", "ElideNone", {
                "ElideNone": "Keep full tab width",
                "ElideMiddle": "Trim the middle of the tab text",
                "ElideLeft": "Trim the left side of the tab text",
                "ElideRight": "Trim the right side of the tab text",
            },
			name="Tab resizing behavior",
			description="This setting determines how tabs adapt when the tab bar runs out of room to display tabs."),
        BoolVar("developerExtras", True, name="Developer extras"),
        BoolVar("plugins", True, name="Enable plugins"),
        BoolVar("offlineStorage", True, name="Offline storage"),
        BoolVar("offlineWebApplicationCache", True, name="Offline web application cache"),
        BoolVar("localStorage", True, name="Local storage"),
        BoolVar("javascriptCanOpenWindows", True, name="JavaScript may open new windows"),
    ]

    typedHistory = None
    visitedHistory = None

    urlChanged = QtCore.pyqtSignal(str)
    iconChanged = QtCore.pyqtSignal(QtGui.QIcon)
    titleChanged = QtCore.pyqtSignal(str)
    loadStarted = QtCore.pyqtSignal()
    loadProgress = QtCore.pyqtSignal(int)
    loadFinished = QtCore.pyqtSignal(bool)

    @classmethod
    def getSettings(cls):
        Settings.createSettings(cls,
			cls.settingsPrefix,
			cls.settingsDef,
			"Tab Behavior",
            QtGui.QIcon(":icons/settings-tabs.png"))
        return cls.settings

    def __init__(self, parent):
        QtCore.QObject.__init__(self, parent)

        self.getSettings()

        self.parent = parent
        self.tabBar = parent.tabBar

        if Tab.typedHistory is None:
            Tab.typedHistory = history.History("typed")

        if Tab.visitedHistory is None:
            Tab.visitedHistory = history.History("visited")

        self.webPage = WebPage(self)
        self.webPage.setNetworkAccessManager(networkcookiejar.getSession())
        webViewSettings = self.webPage.settings()
        webViewSettings.setAttribute(QtWebKit.QWebSettings.DeveloperExtrasEnabled, self.settings["developerExtras"])
        webViewSettings.setAttribute(QtWebKit.QWebSettings.PluginsEnabled, self.settings["plugins"])
        webViewSettings.setAttribute(QtWebKit.QWebSettings.OfflineStorageDatabaseEnabled, self.settings["offlineStorage"])
        webViewSettings.setAttribute(QtWebKit.QWebSettings.OfflineWebApplicationCacheEnabled, self.settings["offlineWebApplicationCache"])
        webViewSettings.setAttribute(QtWebKit.QWebSettings.LocalStorageDatabaseEnabled, self.settings["localStorage"])
        webViewSettings.setAttribute(QtWebKit.QWebSettings.JavascriptCanOpenWindows, self.settings["javascriptCanOpenWindows"])
        webViewSettings.setOfflineStoragePath (".")
        webViewSettings.setIconDatabasePath (".")

        self.webPage.connect(self.webPage.mainFrame(), QtCore.SIGNAL("urlChanged(QUrl)"), self.on_urlChanged)

        self.percentLoaded = 0
        
        if self.settings["tabsOpenRelative"]:
            index = self.tabBar.insertTab(self.tabBar.currentIndex() + 1, self.getIcon(), self.getTitle(), self)
        else:
            index = self.tabBar.addTab(self.getIcon(), self.getTitle(), self)

    @QtCore.pyqtSignature("int")
    def close(self):
        self.tabBar.removeTabByObject(self)

    @QtCore.pyqtSignature("char *")
    def go(self, request):
        matched = False
        for filter in requestfilters.filters:
            (matched, request) = filter.process(request)
            if matched:
                break
        if not matched:
            (matched, request) = requestfilters.defaultFilter.processDefault(request)
            if not matched:
                QtGui.QMessageBox.critical(self.tabBar, "Error",
                    "Couldn't process request '%s'!" % (request, ))
                return
        if request is not None:
            url = QtCore.QUrl(request)
            self.webPage.mainFrame().load(url)
            Tab.typedHistory.addUrl(request)

    @QtCore.pyqtSignature("")
    def stop(self):
        self.webPage.triggerAction(QtWebKit.QWebPage.Stop)

    @QtCore.pyqtSignature("")
    def back(self):
        self.webPage.triggerAction(QtWebKit.QWebPage.Back)

    @QtCore.pyqtSignature("")
    def forward(self):
        self.webPage.triggerAction(QtWebKit.QWebPage.Forward)

    @QtCore.pyqtSignature("")
    def reload(self):
        self.webPage.triggerAction(QtWebKit.QWebPage.Reload)

    @QtCore.pyqtSignature("char *")
    def save(self, file):
        pass

    @QtCore.pyqtSignature("int, int")
    def scroll(self, dx, dy):
        self.webPage.mainFrame().scroll(dx, dy)

    @QtCore.pyqtSignature("")
    def printPage(self):
        pass

    def getHistory(self):
        pass

    @QtCore.pyqtSignature("QUrl")
    def on_urlChanged(self, url):
        Tab.visitedHistory.addUrl(str(url.toString()))

    def requestNewTab(self):
        return self.parent.newTab()


    ### Properties ###
    def getUrl(self):
        return self.webPage.mainFrame().url().toString()

    def setUrl(self, value):
        self.go(value)

    url = property(getUrl, setUrl, doc="The URL of the current page.")


    def getIcon(self):
        return self.webPage.mainFrame().icon()

    icon = property(getIcon, doc="The icon of the current page.")


    def getTitle(self):
        return self.webPage.mainFrame().title()

    title = property(getTitle, doc="The title of the current page.")


    def getTextSize(self):
        return self.widget.textSizeMultiplier()

    def setTextSize(self, value):
        return self.widget.setTextSizeMultiplier(value)

    textSize = property(getTextSize, setTextSize, doc="The text size for the current page.")


    def getZoom(self):
        return self.widget.zoomFactor()

    def setZoom(self, value):
        self.widget.setZoomFactor(value)

    zoom = property(getZoom, setZoom, doc="The zoom of the current page.")


    def getCurrentFrame(self):
        pass

    def setCurrentFrame(self, index):
        pass

    currentFrame = property(getCurrentFrame, setCurrentFrame, doc="The current frame.")

