import urllib
from PyQt4 import QtCore, QtNetwork
from settings import *
import command, history

filters = []
defaultFilter = None
notify = []

def initialize():
    global filters, defaultFilter
    filters = [
        Command(),
        Search(),
        Http(),
        ]
    defaultFilter = filters[0]

def notifyOnUpdate(target):
    global notify
    notify.append(target)

def updated():
    global notify
    for target in notify:
        target.update("requestfilters")


class Search(object):
    settingsPrefix = "commandFilters/searchEngines"
    settingsDef = [
        StringVar("default", "", name="Default search"),
        TableVar("engines", [
            StringVar("name", "", name="Name"),
            StringVar("format", "", description="The format for this search engine's URL. If this is a search engine, put '{terms}' in the URL where the search terms should be inserted.", name="URL"),
            StringVar("shortcuts", "", name="Shortcuts")
        ], name="Shortcuts")
    ]

    def __init__(self):
        Settings.createSettings(self.__class__,
			self.settingsPrefix,
			self.settingsDef,
			"Search Engines and Shortcuts",
			QtGui.QIcon(":icons/settings-shortcuts.png"))
        self.settings.onChanged.append(self.configurationChanged)
        self.configurationChanged()
        self.completionColor = QtGui.qApp.palette().brush(QtGui.QPalette.WindowText).color()
        self.completionColor = QtGui.QColor(
            self.completionColor.red() / 2,
            (self.completionColor.green() + 255) / 2,
            self.completionColor.blue() / 2)
        updated()

    def configurationChanged(self, source = None):
        self.engineFormats = {}
        self.engineShortcuts = {}
        for engine in self.settings["engines"]:
            name = engine["name"].value
            self.engineFormats[name] = engine["format"].value
            for shortcut in engine["shortcuts"].value.split(","):
                if shortcut in self.engineShortcuts:
                    print "WARNING: Duplicate shortcut name used!"
                self.engineShortcuts[shortcut] = name

    def process(self, request):
        if " " not in request.strip():
            return False, request

        engine, rest = request.split(None, 1)
        if engine not in self.engineShortcuts:
            return False, request
        format = self.engineFormats[self.engineShortcuts[engine]]
        return True, format.format(terms=urllib.quote_plus(rest))

    def processDefault(self, request):
        if self.settings["default"] not in self.engineFormats:
            return False, request

        format = self.engineFormats[self.settings["default"]]
        return True, str(format).format(terms=urllib.quote_plus(request))

    def getCompletions(self):
        return [(k + " ", self.engineShortcuts[k], self.completionColor, self.engineShortcuts[k]) for k in self.engineShortcuts]


class Http(object):
    settingsPrefix = "commandFilters/http"
    settingsDef = [
        BoolVar("autoAddScheme", True, name="Automatically add 'http://' to URLs without a scheme")
    ]

    def __init__(self):
        Settings.createSettings(self.__class__,
			self.settingsPrefix,
			self.settingsDef,
			"HTTP URLs",
            QtGui.QIcon(":icons/settings-http.png"))

        self.completionColor = QtGui.qApp.palette().brush(QtGui.QPalette.WindowText).color()
        self.completionColor = QtGui.QColor(
            self.completionColor.red() / 2,
            self.completionColor.green() / 2,
            (self.completionColor.blue() + 255) / 2)

        updated()

    def process(self, request):
        if "://" not in request:
            host = request.split("/")[0].split(":")[0]
            hostInfo = QtNetwork.QHostInfo.fromName(host);
            if hostInfo.error() != QtNetwork.QHostInfo.NoError:
                return False, request
        return self.processDefault(request)

    def processDefault(self, request):
        if self.settings["autoAddScheme"] and "://" not in request:
            request = "http://" + request
        return True, request

    def getCompletions(self):
        completions = list()
        for hist in history.histories:
            completions += [(url, url, self.completionColor, title) for url, title in hist.getLatest()]
        return completions


class Command(object):
    settingsPrefix = "commandFilters/command"
    settingsDef = [
        StringVar("prefix", ":", name="Command prefix", description="When typing a command in the address bar, you must put this prefix before the command name."),
        BoolVar("autoCompleteAliases", True, name="Auto-complete aliases", description="When auto-completing, use aliases as well as standard command names.")
    ]

    def __init__(self):
        Settings.createSettings(self.__class__,
			self.settingsPrefix,
			self.settingsDef,
			"Commands",
            QtGui.QIcon(":icons/settings-commands.png"))
        self.completionColor = QtGui.qApp.palette().brush(QtGui.QPalette.WindowText).color()
        updated()

    def process(self, request):
        return self.processDefault(request)

    def processDefault(self, request):
        prefix = self.settings["prefix"]
        if not request.startswith(prefix):
            return False, request

        cmd = request.split(None, 1)
        params = []
        if len(cmd) > 1:
            params = cmd[1].split()
        cmd = cmd[0]
        cmd = cmd[len(prefix):]

        matches = command.find(cmd)
        if len(matches) == 0:
            return False, request
        elif len(matches) == 1:
            matches[0](*params)
        else:
            #TODO: Put this in the UI somewhere! (dropdown?)
            print "Matched commands:", [m.name for m in matches]

        return True, None

    def getCompletions(self):
        if self.settings["autoCompleteAliases"]:
            return [(self.settings["prefix"] + k + " ",
                command.aliases[k].longName(),
                self.completionColor,
                command.aliases[k].description()) for k in command.aliases]
        else:
            return [(self.settings["prefix"] + k + " ",
                command.commands[k].longName(),
                self.completionColor,
                command.commands[k].description()) for k in command.commands]

