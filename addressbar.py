from PyQt4 import QtGui, QtCore
import completion

class AddressBar(QtGui.QFrame):
    returnPressed = QtCore.pyqtSignal()

    def __init__(self, parent):
        QtGui.QFrame.__init__(self, parent)
        self.parent = parent

        baseColor = self.palette().brush(QtGui.QPalette.Button).color()
        selectionColor = self.palette().brush(QtGui.QPalette.Highlight).color()

        self.normalBorderColor = "palette(button)"
        self.focusedBorderColor = "palette(highlight)"

        self.baseStyle = """
QFrame {{
    background: palette(button);
    border-width: 2px;
    border-style: solid;
    border-color: {borderColor};
    border-radius: 10px;
    padding: 0 1px;
}}"""

        self.progressStyle = """
QProgressBar {
    background: palette(base);
    border: 0;
    border-radius: 0;
    padding: 0;
}

QProgressBar::chunk {
    background: palette(highlight);
    width: 1px;
}"""

        self.statusButtonStyle = """
QToolButton {
    border: none;
    background: none;
    vertical-align: bottom;
}

QToolButton[popupMode="1"] {
    padding-right: 3px;
    margin-right: 2px;
}

QToolButton:pressed {
    background: none;
}

/* the subcontrols below are used only in the MenuButtonPopup mode */
QToolButton::menu-button {
    border: none;
    width: 7px;
    height: 7px;
    margin-left: -4px;
}"""

        self.actionButtonStyle = """
QToolButton {
    border: none;
    background: none;
    vertical-align: bottom;
}

QToolButton[popupMode="1"] {
    padding-right: 3px;
    margin-right: 2px;
}

QToolButton:pressed {
    background: none;
}

/* the subcontrols below are used only in the MenuButtonPopup mode */
QToolButton::menu-button {
    border: none;
    width: 7px;
    height: 7px;
    margin-left: -4px;
}"""

        self.setStyleSheet(self.baseStyle.format(borderColor=self.normalBorderColor))
        self.setFrameShape(QtGui.QFrame.StyledPanel)
        self.setFrameShadow(QtGui.QFrame.Sunken)
        self.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Fixed)
        self.setFocusPolicy(QtCore.Qt.StrongFocus)

        self.layout = QtGui.QHBoxLayout(self)
        self.layout.setSpacing(0)
        self.layout.setMargin(0)

        self.favicon = QtGui.QToolButton(self)
        self.favicon.setStyleSheet("border: none; background: none; border-radius: 0;")
        self.favicon.setIcon(QtGui.QIcon(":icons/go-home.png"))
        self.layout.addWidget(self.favicon)

        self.stack = QtGui.QStackedLayout(self)
        self.stack.setStackingMode(QtGui.QStackedLayout.StackAll)

        self.progress = QtGui.QProgressBar(self)
        self.progress.setStyleSheet(self.progressStyle)
        self.progress.setTextVisible(False)
        self.progress.setRange(0, 100)
        self.progress.setValue(0)
        self.stack.addWidget(self.progress)

        self.sbWidget = QtGui.QWidget(self)

        self.statusButtons = QtGui.QHBoxLayout(self.sbWidget)
        self.statusButtons.setSpacing(0)
        self.statusButtons.setMargin(0)

        class AddressLineEdit(QtGui.QLineEdit):
            def __init__(self, parent):
                QtGui.QLineEdit.__init__(self, parent)
                self.parent = parent
                self.setAttribute(QtCore.Qt.WA_MacShowFocusRect, False)

            def focusInEvent(self, event):
                self.parent.onFocusIn()
                return QtGui.QLineEdit.focusInEvent(self, event)

            def focusOutEvent(self, event):
                self.parent.onFocusOut()
                return QtGui.QLineEdit.focusOutEvent(self, event)

        self.addressEdit = AddressLineEdit(self)
        self.addressEdit.setStyleSheet("border: none; background-color: rgba(0, 0, 0, 0); border-radius: 0;")
        self.addressEdit.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        self.addressEdit.connect(self.addressEdit, QtCore.SIGNAL("returnPressed()"),
            self.on_returnPressed)

        addressBackColor = self.addressEdit.palette().brush(QtGui.QPalette.Base).color()
        self.sbWidget.setStyleSheet("border: none; background-color: rgba(%d, %d, %d, 128); border-radius: 0;" % (addressBackColor.red(), addressBackColor.green(), addressBackColor.blue()))

        completer = QtGui.QCompleter(completion.model, self.addressEdit)
        completer.setModelSorting(QtGui.QCompleter.CaseInsensitivelySortedModel)
        completer.setCaseSensitivity(QtCore.Qt.CaseInsensitive)
        completer.setCompletionColumn(0)
        completer.setCompletionRole(QtCore.Qt.EditRole)
        completer.setCompletionMode(QtGui.QCompleter.PopupCompletion)

        self.addressEdit.setCompleter(completer)

        self.setFocusProxy(self.addressEdit)
        self.statusButtons.addWidget(self.addressEdit)

        self.stack.addWidget(self.sbWidget)

        self.stack.setCurrentIndex(1)
        self.layout.addLayout(self.stack)

        self.actionButtons = QtGui.QHBoxLayout(self)
        self.actionButtons.setSpacing(0)
        self.actionButtons.setMargin(0)
        self.layout.addLayout(self.actionButtons)

    def paintEvent(self, event):
        paint = QtGui.QPainter()
        paint.begin(self)

        paint.end()

        QtGui.QFrame.paintEvent(self, event)

    def setFocus(self, reason):
        self.addressEdit.setFocus(reason)
        self.addressEdit.selectAll()

    def onFocusIn(self):
        self.setStyleSheet(self.baseStyle.format(borderColor=self.focusedBorderColor))

    def onFocusOut(self):
        self.setStyleSheet(self.baseStyle.format(borderColor=self.normalBorderColor))

    @QtCore.pyqtSignature("")
    def on_returnPressed(self):
        self.returnPressed.emit()

    def setText(self, text):
        if 'toString' in dir(text):
            self.addressEdit.setText(str(text.toString()))
        else:
            self.addressEdit.setText(str(text))

    def text(self):
        return self.addressEdit.text()

    def setFavicon(self, icon):
        self.favicon.setIcon(icon)

    def favicon(self):
        return self.favicon.icon()

    def setProgress(self, percent):
        self.progress.setValue(percent)

    def addStatusButton(self, button):
        button.setStyleSheet(self.statusButtonStyle)
        self.statusButtons.addWidget(button)

    def addActionButton(self, button):
        button.setStyleSheet(self.actionButtonStyle)
        self.actionButtons.addWidget(button)

