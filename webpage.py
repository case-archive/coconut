from PyQt4 import QtWebKit

class WebPage(QtWebKit.QWebPage):

    def __init__(self, parent = None):
        self.parent = parent
        QtWebKit.QWebPage.__init__(self)

    def createWindow(self, windowType):
        if (windowType == QtWebKit.QWebPage.WebBrowserWindow):
            if (self.parent != None):
                newtab = self.parent.requestNewTab()
                return newtab.webPage
            else:
                return None
        else:
            if (windowType == QtWebKit.WebModalDialog):
                print "Argh! Modal Windows!"
                return None 

