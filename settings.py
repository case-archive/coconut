from PyQt4 import QtCore, QtGui
import copy

allSettings = list()

class Variable(object):
    def __init__(self, key, default = None, name = None, description = None):
        self.key = key
        self.value = default
        self.newValue = default
        self.name = name
        self.description = description
        self.control = None
        self.onValueChanged = []
        self.onValueCommitted = []

    def load(self, settings):
        self.setValue(settings.value(self.key, QtCore.QVariant(self.value)).toString())
        self.commit()
        return self.value

    def save(self, settings):
        settings.setValue(self.key, QtCore.QVariant(self.value))

    def commit(self):
        self.value = self.newValue
        for callback in self.onValueCommitted:
            callback(self)

    def revert(self):
        self.newValue = self.value
        for callback in self.onValueChanged:
            callback(self)

    def setValue(self, value):
        self.newValue = value
        for callback in self.onValueChanged:
            callback(self)

    def getValue(self):
        return self.value

    def getName(self):
        if self.name is not None:
            return self.name
        else:
            return self.key

class BoolVar(Variable):
    def __init__(self, key, default = False, **kwargs):
        Variable.__init__(self, key, default, **kwargs)

    def load(self, settings):
        self.setValue(settings.value(self.key, QtCore.QVariant(self.value)).toBool())
        self.commit()
        return self.value

    def generateUI(self, parent):
        if self.control is None:
            self.control = QtGui.QCheckBox(parent)
            self.control.connect(self.control, QtCore.SIGNAL("stateChanged(int)"),
                self.onStateChanged)
            if self.description is not None:
                self.control.setToolTip(self.description)
        self.onValueChanged.append(self.populateUI)
        return self.control

    def populateUI(self, *args):
        if self.newValue:
            self.control.setCheckState(QtCore.Qt.Checked)
        else:
            self.control.setCheckState(QtCore.Qt.Unchecked)

    def setValue(self, value):
        Variable.setValue(self, bool(value))

    def onStateChanged(self, state):
        if state == QtCore.Qt.Unchecked:
            self.setValue(False)
        elif state == QtCore.Qt.Checked:
            self.setValue(True)
        else:
            raise Exception("FUCK!")

class EnumVar(Variable):
    def __init__(self, key, default = "", choices = dict(), icons = dict(), choiceSort = None, **kwargs):
        Variable.__init__(self, key, default, **kwargs)
        self.icons = icons
        self.choices = choices
        self.choiceSort = choiceSort
        if choiceSort is None:
            self.choiceSort = self.choices.keys()
            self.choiceSort.sort()

    def __deepcopy__(self, memo):
        return copy.copy(EnumVar(self.key, copy.copy(self.value), self.choices, self.icons, self.choiceSort))

    def load(self, settings):
        self.setValue(settings.value(self.key, QtCore.QVariant(self.value)).toString())
        if self.newValue not in self.choices:
            print "WARNING: Invalid value '%s' for config var '%s'! Reverting to '%s'." % (self.newValue, self.key, self.value)
            self.revert()
        else:
            self.commit()
        return self.value

    def revert(self):
        if self.value not in self.choices:
            self.value = self.choices[self.choices.keys[0]]
        Variable.revert(self)

    def generateUI(self, parent):
        if self.control is None:
            self.control = QtGui.QComboBox(parent)
            self.control.setSizePolicy(QtGui.QSizePolicy.MinimumExpanding, QtGui.QSizePolicy.Fixed)
            self.control.connect(self.control, QtCore.SIGNAL("currentIndexChanged(int)"),
                self.onIndexChanged)
            if self.description is not None:
                self.control.setToolTip(self.description)
        self.onValueChanged.append(self.populateUI)
        return self.control

    def populateUI(self, *args):
        self.control.disconnect(self.control, QtCore.SIGNAL("currentIndexChanged(int)"),
            self.onIndexChanged)
        self.control.clear()
        for choice in self.choiceSort:
            if choice in self.icons:
                self.control.addItem(self.icons[choice], self.choices[choice], QtCore.QVariant(choice))
            else:
                self.control.addItem(self.choices[choice], QtCore.QVariant(choice))
        if self.newValue in self.choices:
            self.control.setCurrentIndex(self.control.findData(QtCore.QVariant(self.newValue)))
        self.control.connect(self.control, QtCore.SIGNAL("currentIndexChanged(int)"),
            self.onIndexChanged)

    @QtCore.pyqtSignature("int")
    def onIndexChanged(self, index):
        self.setValue(str(self.control.itemData(index).toString()))

    def setValue(self, value):
        Variable.setValue(self, str(value))

class StringVar(Variable):
    def __init__(self, key, default = "", **kwargs):
        Variable.__init__(self, key, default, **kwargs)

    def load(self, settings):
        self.setValue(settings.value(self.key, QtCore.QVariant(self.value)).toString())
        self.commit()
        return self.value

    def generateUI(self, parent):
        if self.control is None:
            self.control = QtGui.QLineEdit(parent)
            self.control.connect(self.control, QtCore.SIGNAL("editingFinished()"),
                self.setValue)
            if self.description is not None:
                self.control.setToolTip(self.description)
        self.onValueChanged.append(self.populateUI)
        return self.control

    def populateUI(self, *args):
        self.control.setText(self.newValue)

    def setValue(self, text = None):
        if text is None:
            Variable.setValue(self, str(self.control.text()))
        else:
            Variable.setValue(self, str(text))

class TableVar(Variable):
    def __init__(self, key, fields = None, **kwargs):
        Variable.__init__(self, key, **kwargs)
        self.fields = list()
        if fields is not None:
            self.fields = list(fields)
        self.value = list()
        self.additions = list()
        self.removals = list()

    def addField(self, field):
        self.fields.append(field)

    def load(self, settings):
        self.value = list()
        size = settings.beginReadArray(self.key)
        for i in range(size):
            settings.setArrayIndex(i)
            line = dict()
            for field in self.fields:
                line[field.key] = copy.deepcopy(field)
                line[field.key].load(settings)
            self.value.append(line)
        settings.endArray()
        self.additions = list()
        self.removals = list()
        return self.value

    def save(self, settings):
        settings.beginWriteArray(self.key)
        index = 0
        for row in self.value:
            settings.setArrayIndex(index)
            for field in self.fields:
                row[field.key].save(settings)
            index += 1
        settings.endArray()

    def generateUI(self, parent):
        if self.control is None:
            self.control = QtGui.QWidget(parent)
            self.control.setSizePolicy(QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
            if self.description is not None:
                self.control.setToolTip(self.description)

            layout = QtGui.QGridLayout()

            self.table = QtGui.QTableWidget(self.control)
            self.table.setColumnCount(len(self.fields))
            self.table.setHorizontalHeaderLabels([field.getName() for field in self.fields])
            self.table.resizeRowsToContents()
            layout.addWidget(self.table, 0, 0, 1, 3)

            self.addButton = QtGui.QToolButton()
            self.addButton.setIcon(QtGui.QIcon(":icons/list-add.png"))
            self.addButton.setText("Add")
            self.addButton.setToolTip("Add")
            layout.addWidget(self.addButton, 1, 1)

            self.removeButton = QtGui.QToolButton()
            self.removeButton.setIcon(QtGui.QIcon(":icons/list-remove.png"))
            self.removeButton.setText("Remove")
            self.removeButton.setToolTip("Remove")
            layout.addWidget(self.removeButton, 1, 2)

            self.addButton.connect(self.addButton, QtCore.SIGNAL("clicked()"),
                self.onAdd)
            self.removeButton.connect(self.removeButton, QtCore.SIGNAL("clicked()"),
                self.onRemove)
            self.table.connect(self.table, QtCore.SIGNAL("cellChanged(int, int)"),
                self.onCellChanged)

            self.control.setLayout(layout)
        self.onValueChanged.append(self.populateUI)
        return self.control

    def populateUI(self, *args):
        if len(self.value) != self.table.rowCount():
            self.table.setRowCount(len(self.value))
            rowidx = 0
            for row in self.value:
                colidx = 0
                for field in self.fields:
                    control = row[field.key].generateUI(self.table)
                    if 'setFrame' in dir(control):
                        control.setFrame(False)
                    self.table.setCellWidget(rowidx, colidx, control)
                    row[field.key].populateUI()
                    row[field.key].onValueChanged.append(lambda x: self.onCellChanged(rowidx, colidx))
                    colidx += 1
                rowidx += 1
            if self.table.rowCount() > 0:
                self.table.setFixedHeight(8 * self.table.rowHeight(1))

    def commit(self):
        self.additions = list()

        # Remove deleted rows
        self.removals.sort(reverse=True)
        for rowidx in self.removals:
            row = self.value[rowidx]
            for field in self.fields:
                del row[field.key]
            del self.value[rowidx]
        self.removals = list()

        for row in self.value:
            for field in self.fields:
                row[field.key].commit()

        for callback in self.onValueCommitted:
            callback(self)

    def revert(self):
        # Remove newly-created rows
        self.additions.sort(reverse=True)
        for rowidx in self.additions:
            row = self.value[rowidx]
            for field in self.fields:
                del row[field.key]
            del self.value[rowidx]
        self.additions = list()

        self.removals = list()

        for row in self.value:
            for field in self.fields:
                row[field.key].revert()

        for callback in self.onValueChanged:
            callback(self)

    def setValue(self, value):
        raise Exception("Can't set the value of a table!")

    def getValue(self):
        return self.value

    @QtCore.pyqtSignature("")
    def onAdd(self):
        newrow = self.table.rowCount()
        self.table.setRowCount(newrow + 1)
        line = dict()
        fieldIdx = 0
        for field in self.fields:
            line[field.key] = copy.deepcopy(field)
            self.table.setCellWidget(newrow, fieldIdx, line[field.key].generateUI(self.table))
            line[field.key].populateUI()
            fieldIdx += 1

        self.additions.append(len(self.value))
        self.value.append(line)

    @QtCore.pyqtSignature("")
    def onRemove(self):
        #FIXME: This doesn't actually work...
        curRow = self.table.currentRow()
        self.table.removeRow(curRow)
        self.removals.append(curRow)
        #FIXME: Remove the actual config var as well, but only on commit!

    @QtCore.pyqtSignature("int, int")
    def onCellChanged(self, row, column):
        for callback in self.onValueChanged:
            callback(self)


class Settings(object):
    @classmethod
    def createSettings(cls, target, *args):
        if 'settings' not in dir(target):
            target.settings = cls(*args)
            target.settings.load()

    def __init__(self, prefix, options, name, icon = None, toolTip = None):
        global allSettings
        allSettings.append(self)
        self.prefix = prefix
        self.options = options
        self.name = name
        self.icon = icon
        self.toolTip = toolTip
        self.optionIndex = dict()
        self.onChanged = list()
        self.onSaveStarted = list()
        self.onSaveFinished = list()
        for var in self.options:
            self.optionIndex[var.key] = var
            var.onValueCommitted.append(self.changed)

    def load(self):
        qSettings = QtCore.QSettings()
        if self.prefix is not None:
            qSettings.beginGroup(self.prefix)

        for var in self.options:
            var.load(qSettings)

        if self.prefix is not None:
            qSettings.endGroup()
        self.changed()

    def save(self):
        self.saveStarted()
        qSettings = QtCore.QSettings()
        if not qSettings.isWritable():
            raise Exception("Couldn't write settings!")
        if self.prefix is not None:
            qSettings.beginGroup(self.prefix)

        for var in self.options:
            var.save(qSettings)

        if self.prefix is not None:
            qSettings.endGroup()
        self.saveFinished()

    def __getitem__(self, key):
        if key not in self.optionIndex:
            raise Exception("Invalid option key: " + str(key))
        return self.optionIndex[key].getValue()

    def __setitem__(self, key, value):
        if key not in self.optionIndex:
            raise Exception("Invalid option key: " + str(key))
        self.optionIndex[key].setValue(value)

    def commit(self, key):
        if key not in self.optionIndex:
            raise Exception("Invalid option key: " + str(key))
        self.optionIndex[key].commit()

    @QtCore.pyqtSignature("")
    def apply(self):
        for var in self.options:
            var.commit()
        self.save()

    @QtCore.pyqtSignature("")
    def revert(self):
        for var in self.options:
            var.revert()

    def generateConfigUI(self, parent):
        accelerators = set()

        def addAccelerator(label, accelerators):
            for i in range(len(label)):
                if label[i] not in accelerators:
                    accelerators.add(label[i])
                    return label[:i] + "&" + label[i:]

        applyLabel = addAccelerator("Apply", accelerators)
        revertLabel = addAccelerator("Revert", accelerators)

        page = QtGui.QWidget(parent)
        formLayout = QtGui.QFormLayout(page)
        formLayout.setFieldGrowthPolicy(QtGui.QFormLayout.AllNonFixedFieldsGrow)
        for var in self.options:
            control = var.generateUI(page)
            label = addAccelerator(var.getName(), accelerators)
            if type(var) is TableVar:
                # Special cases! YUCK!
                labelWidget = QtGui.QLabel(label + ":")
                labelWidget.setBuddy(control)
                labelWidget.setAlignment(QtCore.Qt.AlignLeft)

                subLayout = QtGui.QVBoxLayout()
                subLayout.setSpacing(0)

                subLayout.addWidget(labelWidget)
                subLayout.addWidget(control)
                formLayout.addRow(subLayout)
            else:
                formLayout.addRow(label, control)

        buttonLayout = QtGui.QHBoxLayout()

        self.applyButton = QtGui.QPushButton()
        self.applyButton.setIcon(QtGui.QIcon(":icons/dialog-ok-apply.png"))
        self.applyButton.setText(applyLabel)
        self.applyButton.setToolTip("Apply")
        buttonLayout.addWidget(self.applyButton)

        self.revertButton = QtGui.QPushButton()
        self.revertButton.setIcon(QtGui.QIcon(":icons/dialog-cancel.png"))
        self.revertButton.setText(revertLabel)
        self.revertButton.setToolTip("Revert")
        buttonLayout.addWidget(self.revertButton)

        self.applyButton.connect(self.applyButton, QtCore.SIGNAL("clicked()"),
            self.apply)
        self.revertButton.connect(self.revertButton, QtCore.SIGNAL("clicked()"),
            self.revert)

        formLayout.addRow(buttonLayout)

        page.setLayout(formLayout)
        return page

    def populateConfigUI(self):
        for var in self.options:
            var.populateUI()

    def changed(self, source = None):
        for callback in self.onChanged:
            callback(source)

    def saveStarted(self):
        print "Started saving configuration..."
        for callback in self.onSaveStarted:
            callback()

    def saveFinished(self):
        print "Finished saving configuration."
        for callback in self.onSaveFinished:
            callback()

