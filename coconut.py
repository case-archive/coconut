#!/usr/bin/env python

import sys, os.path
from PyQt4 import QtCore, QtGui, QtWebKit
import mainbrowser_rc
import tab, shortcuts, requestfilters, addressbar, command, networkcookiejar, downloads, tabbar, completion
from settings import *
from command import Command


app = QtGui.QApplication(sys.argv)

QtCore.QCoreApplication.setOrganizationName("G33X Nexus Enterprises");
QtCore.QCoreApplication.setOrganizationDomain("g33xnexus.com");
QtCore.QCoreApplication.setApplicationName("Coconut");


class MainWindow(QtGui.QMainWindow):
    settingsPrefix = "general"
    settingsDef = [
        BoolVar("saveWindowSize", True, name="Automatically save window size on exit"),
        BoolVar("saveWindowPosition", True, name="Automatically save window position on exit"),
        StringVar("windowSize", "", name="Initial window size", description="Set the window's size on startup. To disable, delete the value."),
        StringVar("windowPosition", "", name="Initial window position", description="Set the window's position on startup. To disable, delete the value."),
    ]

    def __init__(self, *args):
        QtGui.QMainWindow.__init__(self, *args)
        self.setObjectName("MainCoconutWindow")

        Settings.createSettings(self.__class__,
			self.settingsPrefix,
			self.settingsDef,
			"General",
			QtGui.QIcon(":icons/settings-general.png"))

        self.setWindowTitle("Coconut")
        self.setWindowIcon(QtGui.QIcon(":icons/coconut-16.png"))
        self.setAttribute(QtCore.Qt.WA_MacBrushedMetal, True)

        size = self.settings["windowSize"]
        if size != "":
            width, height = size.split(",", 1)
            self.resize(int(width), int(height))

        position = self.settings["windowPosition"]
        if position != "":
            x, y = position.split(",", 1)
            self.move(int(x), int(y))

        self.registerCommands()

        requestfilters.initialize()
        completion.initialize(self)

        # Create the tabs collection.
        self.lastTab = None

        self.createWidgets()

        self.connectAll()

        self.shortcuts = shortcuts.KeyboardShortcuts(self)

        downloads.initialize(self.downloadMenu.action(self))

        # Create a new tab.
        self.newTab()
        #self.tabBar.removeTab(0)

    def registerCommands(self):
        self.back.register()
        self.forward.register()
        self.refresh.register()
        self.stop.register()
        self.bookmark.register()
        self.pageMenu.register()
        self.bookmarkMenu.register()
        self.goHome.register()
        self.open.register()
        self.copyLink.register()
        self.quit.register()
        self.settingsMenu.register()
        self.downloadMenu.register()
        self.focusAddressBar.register()
        self.focusPage.register()
        self.scrollUp.register()
        self.scrollDown.register()
        self.scrollLeft.register()
        self.scrollRight.register()
        self.newTab.register()
        self.switchToTab.register()
        self.switchToLastTab.register()
        self.switchToNextTab.register()
        self.switchToPreviousTab.register()
        self.closeTab.register()

    def createWidgets(self):
        # Create the layout of the main window.
        self.centralWidget = QtGui.QWidget(self)
        self.setCentralWidget(self.centralWidget)

        self.centralLayout = QtGui.QVBoxLayout(self.centralWidget)
        self.centralLayout.setSpacing(0)
        self.centralLayout.setContentsMargins(0, 0, 0, 0)

        self.topBar = QtGui.QHBoxLayout(self.centralWidget)
        self.topBar.setSpacing(0)
        self.topBar.setContentsMargins(0, 0, 0, 0)

        # Create the new tab button.
        self.newTabButton = QtGui.QToolButton(self)
        self.newTabButton.setDefaultAction(self.newTab.action(self))
        self.topBar.addWidget(self.newTabButton)

        # Create the tab bar.
        self.tabBar = tabbar.TabBar(self)
        self.tabBar.setMovable(True)
        self.tabBar.setTabsClosable(True)
        self.tabBar.setUsesScrollButtons(True)
        self.tabBar.setElideMode({
                "ElideNone": QtCore.Qt.ElideNone, 
                "ElideMiddle": QtCore.Qt.ElideMiddle,
                "ElideLeft": QtCore.Qt.ElideLeft,
                "ElideRight": QtCore.Qt.ElideRight,
            }[tab.Tab.getSettings()["textElideMode"]])
        self.topBar.addWidget(self.tabBar)

        self.centralLayout.addLayout(self.topBar)

        # Create the web view.
        self.webView = QtWebKit.QWebView(self)
        self.centralLayout.addWidget(self.webView)

        # Create the navigation toolbar.
        self.navigationToolBar = QtGui.QToolBar("Navigation", self)
        self.navigationToolBar.setAllowedAreas(QtCore.Qt.BottomToolBarArea | QtCore.Qt.TopToolBarArea)
        self.navigationToolBar.setContentsMargins(0, 0, 0, 0)
        self.navigationToolBar.setStyleSheet("padding: 1px;")

        # Create the back, forward, refresh, and stop buttons.
        self.navigationToolBar.addAction(self.back.action(self))
        self.navigationToolBar.addAction(self.forward.action(self))
        self.navigationToolBar.addAction(self.refresh.action(self))
        self.navigationToolBar.addAction(self.stop.action(self))

        # Create the address bar.
        self.addressBar = addressbar.AddressBar(self)
        self.navigationToolBar.addWidget (self.addressBar)

        # Create the security level status icon.
        self.secureStatus = QtGui.QToolButton()
        secureIcon = QtGui.QIcon(":icons/page-secure.png")
        secureIcon.addFile(":icons/page-insecure.png", QtCore.QSize(), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.secureStatus.setIcon(secureIcon)
        self.secureStatus.setText("Secure page?")
        self.addressBar.addStatusButton(self.secureStatus)

        # Create the bookmark status icon.
        self.bookmarkStatus = QtGui.QToolButton()
        self.bookmarkStatus.setDefaultAction(self.bookmark.action(self))
        self.addressBar.addStatusButton(self.bookmarkStatus)

        # Create the page menu button.
        self.page = QtGui.QToolButton()
        self.page.setDefaultAction(self.pageMenu.action(self))
        self.addressBar.addActionButton(self.page)

        # Create the Go button.
        self.go = QtGui.QToolButton()
        self.go.setDefaultAction(self.open.action(self))
        self.addressBar.addActionButton(self.go)

        # Create the bookmark menu button and the settings button.
        self.navigationToolBar.addAction(self.downloadMenu.action(self))
        self.navigationToolBar.addAction(self.bookmarkMenu.action(self))
        self.navigationToolBar.addAction(self.settingsMenu.action(self))

        # Add the toolbar to the main window.
        #TODO: Make the toolbar position configurable!
        self.addToolBar(QtCore.Qt.BottomToolBarArea, self.navigationToolBar)

        # Initially, don't worry about creating the settings pane.
        self.settingsPane = None

    def connectAll(self):
        # Connect signals.
        self.addressBar.returnPressed.connect(self.open)

        self.webView.connect(self.webView, QtCore.SIGNAL("urlChanged(QUrl)"),
            self.on_urlChanged)
        self.webView.connect(self.webView, QtCore.SIGNAL("iconChanged()"),
            self.on_iconChanged)
        self.webView.connect(self.webView, QtCore.SIGNAL("titleChanged(QString)"),
            self.on_titleChanged)
        self.webView.connect(self.webView, QtCore.SIGNAL("loadStarted()"),
            self.on_loadStarted)
        self.webView.connect(self.webView, QtCore.SIGNAL("loadProgress(int)"),
            self.on_loadProgress)
        self.webView.connect(self.webView, QtCore.SIGNAL("loadFinished(bool)"),
            self.on_loadFinished)

        self.tabBar.connect(self.tabBar, QtCore.SIGNAL("currentChanged(int)"),
            self.on_tabBar_currentChanged)
        self.tabBar.connect(self.tabBar, QtCore.SIGNAL("tabCloseRequested(int)"),
            self.on_tabBar_tabCloseRequested)


    ## Navigation Commands ##
    @Command(name="back", longName="Back",
        description="Return to the previous page viewed in this tab",
        icon=QtGui.QIcon(":/icons/go-previous.png"),
        category="Navigation")
    def back(self):
        self.tabBar.currentTab().back()

    @Command(name="forward", longName="Forward",
        description="Move forward to the next page viewed in this tab",
        icon=QtGui.QIcon(":/icons/go-next.png"),
        category="Navigation")
    def forward(self):
        self.tabBar.currentTab().forward()

    @Command(name="refresh", longName="Refresh",
        description="Refresh the current page",
        icon=QtGui.QIcon(":/icons/view-refresh.png"),
        category="Navigation")
    def refresh(self):
        self.tabBar.currentTab().reload()

    @Command(name="stop", longName="Stop",
        description="Stop loading",
        icon=QtGui.QIcon(":/icons/process-stop.png"),
        category="Navigation")
    def stop(self):
        self.tabBar.currentTab().stop()

    bookmarkIcon = QtGui.QIcon(":/icons/bookmarked.png")
    bookmarkIcon.addFile(":/icons/not-bookmarked.png", QtCore.QSize(), QtGui.QIcon.Normal, QtGui.QIcon.Off)

    @Command(name="bookmark", longName="Toggle bookmark",
        aliases=("bmark", ),
        description="Toggle bookmark",
        icon=bookmarkIcon,
        category="Navigation")
    def bookmark(self):
        raise Exception("UNIMPLEMENTED!") #FIXME

    @Command(name="page", longName="Page actions",
        description="Control the current page",
        icon=QtGui.QIcon(":/icons/page-settings.png"),
        category="Navigation")
    def pageMenu(self):
        raise Exception("UNIMPLEMENTED!") #FIXME

    @Command(name="bookmarks", longName="Open bookmark menu",
        aliases=("bmarks", ),
        description="View your bookmarks",
        icon=QtGui.QIcon(":/icons/bookmark-menu.png"),
        category="Navigation")
    def bookmarkMenu(self):
        raise Exception("UNIMPLEMENTED!") #FIXME

    @Command(name="home", longName="Home",
        description="Go to your home page",
        icon=QtGui.QIcon(":/icons/go-home.png"),
        category="Navigation")
    def goHome(self):
        #TODO: Make this a config option.
        self.open("http://www.google.com")

    @Command(name="go", longName="Go",
        aliases=("open", ),
        description="Go to the given URL (defaults to the contents of the address bar)",
        icon=QtGui.QIcon(":/icons/go.png"),
        category="Navigation")
    def open(self):
        request = str(self.addressBar.text())
        self.tabBar.currentTab().go(request)

    @Command(name="copylink", longName="Copy URL",
        aliases=("cl", ),
        description="Copy the URL of the current page to the clipboard",
        icon=QtGui.QIcon(":/icons/copy-url.png"),
        category="Navigation")
    def copyLink(self):
        #FIXME: Doesn't copy to the middle-click clipboard on X11?
        QtGui.QApplication.clipboard().setText(self.tabBar.currentTab().getUrl())

    ## Browser Commands ##
    @Command(name="quit", longName="Quit",
        aliases=("wqa", "exit"),
        description="Exit the browser",
        icon=QtGui.QIcon(":/icons/application-exit.png"),
        category="Browser")
    def quit(self):
        self.close()
        QtGui.qApp.quit()

    @Command(name="settings", longName="Settings",
        aliases=("config", "preferences", "prefs"),
        description="Change the browser's settings",
        icon=QtGui.QIcon(":/icons/settings.png"),
        category="Browser")
    def settingsMenu(self):
        if self.settingsPane is None:
            pane = QtGui.QDockWidget("Settings", self)
            pane.setAllowedAreas(QtCore.Qt.LeftDockWidgetArea | QtCore.Qt.RightDockWidgetArea)
            tabs = QtGui.QToolBox(pane)
            pane.setWidget(tabs)
            self.addDockWidget(QtCore.Qt.RightDockWidgetArea, pane)
            self.settingsPane = pane
            self.settingsTabs = tabs

            for obj in allSettings:
                idx = self.settingsTabs.addItem(obj.generateConfigUI(self.settingsTabs), obj.name)
                if obj.icon is not None:
                    self.settingsTabs.setItemIcon(idx, obj.icon)
                if obj.toolTip is not None:
                    self.settingsTabs.setItemToolTip(idx, obj.toolTip)

        elif self.settingsPane.isVisible():
            self.settingsPane.hide()
            return

        for obj in allSettings:
            obj.populateConfigUI()
        self.settingsPane.show()

    downloadIcon = QtGui.QIcon(":/icons/download.png")
    downloadIcon.addFile(":/icons/download-inactive.png", QtCore.QSize(), QtGui.QIcon.Normal, QtGui.QIcon.Off)

    @Command(name="download", longName="Downloads",
        description="Manage downloads",
        icon=downloadIcon,
        category="Browser")
    def downloadMenu(self):
        raise Exception("UNIMPLEMENTED!") #FIXME

    @Command(name="focusaddressbar", longName="Focus the address bar",
        aliases=("fab", ),
        description="Move the keyboard focus to the address bar",
        icon=QtGui.QIcon(":/icons/browser.png"),
        category="Browser")
    def focusAddressBar(self, text=None):
        self.addressBar.setFocus(QtCore.Qt.OtherFocusReason)
        if text != None:
            self.addressBar.setText(text)

    @Command(name="focuspage", longName="Focus the current page",
        aliases=("fp", ),
        description="Move the keyboard focus to the current page",
        icon=QtGui.QIcon(":/icons/browser.png"),
        category="Browser")
    def focusPage(self):
        self.webView.setFocus(QtCore.Qt.OtherFocusReason)

    ## Page Commands ##
    @Command(name="scrollup", longName="Scroll up",
        aliases=("su", ),
        description="Scroll the current page up",
        icon=QtGui.QIcon(":/icons/scroll-up.png"),
        category="Page")
    def scrollUp(self, speed=1):
        self.tabBar.currentTab().scroll(0, -10 * float(speed))

    @Command(name="scrolldown", longName="Scroll down",
        aliases=("sd", ),
        description="Scroll the current page down",
        icon=QtGui.QIcon(":/icons/scroll-down.png"),
        category="Page")
    def scrollDown(self, speed=1):
        self.tabBar.currentTab().scroll(0, 10 * float(speed))

    @Command(name="scrollleft", longName="Scroll left",
        aliases=("sl", ),
        description="Scroll the current page left",
        icon=QtGui.QIcon(":/icons/scroll-left.png"),
        category="Page")
    def scrollLeft(self, speed=1):
        self.tabBar.currentTab().scroll(-10 * float(speed), 0)

    @Command(name="scrollright", longName="Scroll right",
        aliases=("sr", ),
        description="Scroll the current page right",
        icon=QtGui.QIcon(":/icons/scroll-right.png"),
        category="Page")
    def scrollRight(self, speed=1):
        self.tabBar.currentTab().scroll(10 * float(speed), 0)

    ## Tab Commands ##
    @Command(name="opentab", longName="New tab",
        aliases=("ot", "tabopen", "to"),
        description="Create a new tab and switch to it",
        icon=QtGui.QIcon(":/icons/tab-new.png"),
        category="Tabs")
    def newTab(self, *args):
        newtab = tab.Tab(self)
        #TODO: If focus new tabs (possibly split this into 2 commands)
        self.switchToTab(self.tabBar.indexOfTab(newtab))
        if len(args) > 0:
            newtab.go(" ".join(args))
        else:
            self.focusAddressBar()
        return newtab

    @Command(name="tab", longName="Switch tab",
        aliases=("buffer", ),
        description="Switch to the given tab",
        icon=QtGui.QIcon(":/icons/tabs.png"),
        category="Tabs")
    def switchToTab(self, index):
        self.tabBar.setCurrentIndex(int(index))

        # Redundant, but this fixes the issue that currentChanged is triggered early when a new tab causes the event.
        self.on_tabBar_currentChanged(index)

        self.addressBar.setProgress(self.tabBar.currentTab().percentLoaded)

    @Command(name="lasttab", longName="Last tab",
        aliases=("lt", "tl"),
        description="Switch to the last tab",
        icon=QtGui.QIcon(":/icons/tabs.png"),
        category="Tabs")
    def switchToLastTab(self):
        self.switchToTab(self.tabBar.count() - 1)

    @Command(name="nexttab", longName="Next tab",
        aliases=("nt", "tn", "bn"),
        description="Switch to the next tab",
        icon=QtGui.QIcon(":/icons/tabs.png"),
        category="Tabs")
    def switchToNextTab(self):
        idx = (self.tabBar.currentIndex() + 1) % self.tabBar.count()
        self.switchToTab(idx)

    @Command(name="previoustab", longName="Previous tab",
        aliases=("prevtab", "pt", "tp", "bp"),
        description="Switch to the next tab",
        icon=QtGui.QIcon(":/icons/tabs.png"),
        category="Tabs")
    def switchToPreviousTab(self):
        idx = (self.tabBar.currentIndex() - 1) % self.tabBar.count()
        self.switchToTab(idx)

    @Command(name="closetab", longName="Close tab",
        aliases=("ct", "tc", "bd"),
        description="Close the current tab",
        icon=QtGui.QIcon(":/icons/tab-close.png"),
        category="Tabs")
    def closeTab(self, index = None):
        print "Closing tab", index
        if index is None:
            index = self.tabBar.currentIndex()
        if index < 0 or index > self.tabBar.count():
            raise Exception("Tab index out of range!")

        # Catch the closing of the last tab, and create a new tab to take it's place.
        if self.tabBar.count() == 1:
            self.newTab()

        self.tabBar.tabObject(index).close()


    ## Slots and Event Handlers ##
    def closeEvent(self, event):
        if self.settings["saveWindowSize"]:
            self.settings["windowSize"] = "%d,%d" % (self.width(), self.height())
            self.settings.commit("windowSize")
        if self.settings["saveWindowPosition"]:
            self.settings["windowPosition"] = "%d,%d" % (self.x(), self.y())
            self.settings.commit("windowPosition")
        self.settings.save()
        networkcookiejar.cookieJar.saveCookiesToDisk()

    @QtCore.pyqtSignature("int")
    def on_tabBar_currentChanged(self, index = 0):
        self.lastTab = self.tabBar.currentTab()

        if self.lastTab is not None:
            self.webView.setPage(self.lastTab.webPage)
            self.on_urlChanged(self.lastTab.url)
            self.on_iconChanged(self.lastTab.icon)
            self.on_titleChanged(self.lastTab.title)
            if self.lastTab.percentLoaded == 100:
                self.on_loadFinished(True)
            else:
                self.on_loadProgress(self.lastTab.percentLoaded)

    @QtCore.pyqtSignature("int")
    def on_tabBar_tabCloseRequested(self, index):
        self.closeTab(index)

    @QtCore.pyqtSignature("char *")
    def on_urlChanged(self, url):
        self.addressBar.setText(url)

    @QtCore.pyqtSignature("QIcon")
    def on_iconChanged(self, icon = None):
        if icon is None:
            icon = self.webView.icon()
        self.tabBar.setTabIcon(self.tabBar.currentIndex(), icon)
        self.addressBar.setFavicon(icon)

    @QtCore.pyqtSignature("char *")
    def on_titleChanged(self, title):
        self.setWindowTitle(title)
        self.tabBar.setTabText(self.tabBar.currentIndex(), title)

    @QtCore.pyqtSignature("")
    def on_loadStarted(self):
        self.addressBar.setProgress(0)

    @QtCore.pyqtSignature("int")
    def on_loadProgress(self, status):
        self.addressBar.setProgress(status)

    @QtCore.pyqtSignature("bool")
    def on_loadFinished(self, flag):
        if flag:
            self.addressBar.setProgress(100)
        else:
            self.addressBar.setProgress(0)

        self.on_iconChanged(self.tabBar.currentTab().icon)


mainWindow = MainWindow()
mainWindow.show()

sys.exit(app.exec_())
