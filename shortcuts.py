from PyQt4 import QtCore
from settings import *
import command

class KeyboardShortcuts():
    settingsPrefix = None

    def __init__(self, window):
        self.window = window
        self.shortcuts = []

        choices = dict()
        icons = dict()
        for key, cmd in command.commands.iteritems():
            choices[key] = cmd.longName()
            if cmd.icon() is not None:
                icons[key] = cmd.icon()

        choiceSort = list()
        categories = command.categories.keys()
        categories.sort()
        for category in categories:
            thiscat = [cmd.name() for cmd in command.categories[category]]
            thiscat.sort()
            choiceSort.extend(thiscat)

        self.settingsDef = [
            TableVar("keyBindings", [
                StringVar("key", "", name="Key Combination", description="""To bind to a key combination, simply combine modifiers and keys with a '+'.
The available modifiers are:
    Ctrl
    Alt
    Shift
    Meta (the Windows key)"""),
                EnumVar("slot",
                    default=command.commands.keys()[0],
                    choices=choices,
                    icons=icons,
                    choiceSort=choiceSort,
                    name="Action"),
                BoolVar("autoRepeat", False, name="Auto-repeat", description="Sets whether the command should auto-repeat when you hold down the key combination."),
                StringVar("params", "", name="Parameters", description="Some actions can handle parameters; put them here. See the code for more details."),
                #TODO: Telling the user to see the code is bad form. Is there any way to put this info in the UI?
            ], name="Key bindings"),
            StringVar("paramSeparator", ",", name="Parameter separator", description="This character is used to split the parameters for each key binding."),
        ]
        Settings.createSettings(self.__class__,
			self.settingsPrefix,
			self.settingsDef,
			"Keyboard Shortcuts",
            QtGui.QIcon(":icons/settings-keybindings.png"))
        self.settings.onChanged.append(self.configurationChanged)
        self.configurationChanged()

    def configurationChanged(self, source = None):
        for idx in range(len(self.shortcuts) - 1, -1, -1):
            self.shortcuts[idx].setEnabled(False)
            del self.shortcuts[idx]

        class Binder(object):
            def __init__(self, callable, params):
                self.callable = callable
                self.params = params

            def __call__(self):
                self.callable(*self.params)

        for shortcut in self.settings["keyBindings"]:
            key = shortcut["key"].getValue()
            slot = shortcut["slot"].getValue()
            autoRepeat = shortcut["autoRepeat"].getValue()
            params = shortcut["params"].getValue()
            if params != "":
                params = params.split(self.settings["paramSeparator"])
            if slot in command.commands:
                shortcut = QtGui.QShortcut(QtGui.QKeySequence(key), self.window)
                shortcut.setAutoRepeat(autoRepeat)
                action = Binder(command.commands[slot], params)
                shortcut.connect(shortcut, QtCore.SIGNAL("activated()"),
                    action)
                self.shortcuts.append(shortcut)
            else:
                print "Error: Slot '%s' doesn't exist!" % (slot, )


