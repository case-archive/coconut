from PyQt4 import QtGui
import sqlite3, time, os.path

histories = []
notify = []

def notifyOnUpdate(target):
    global notify
    notify.append(target)

def updated(items = None):
    global notify
    for target in notify:
        target.update("history", items)

class History(object):
    def __init__(self, tag, databasePath = "history.db"):
        global histories
        histories.append(self)

        self.tag = tag
        self.databasePath = databasePath

        dbexists = os.path.exists(databasePath)
        self.db = sqlite3.connect(databasePath)
        if not dbexists:
            c = self.db.cursor()
            c.execute('create table history (tag text, datetime text, url text, title text)')

            self.db.commit()
            c.close()
        updated()

    def addUrl(self, url, title = ""):
        c = self.db.cursor()
        strippedurl = url.rstrip("/")
        if strippedurl != url:
            c.execute("delete from history where tag=? and (url=? or url=?)", (self.tag, url, strippedurl))
        else:
            c.execute("delete from history where tag=? and url=?", (self.tag, url))
        c.execute("insert into history values (?, ?, ?, ?)", (self.tag, time.time(), url, title))
        self.db.commit()
        c.close()
        updated([(url, title, QtGui.QColor(), title)])

    def getLatest(self, maxResults = None, ignore = set()):
        c = self.db.cursor()
        if maxResults is not None:
            c.execute('select url, title from history where tag=? order by datetime desc limit ?', (self.tag, maxResults + len(ignore)))
        else:
            c.execute('select url, title from history where tag=? order by datetime desc', (self.tag, ))
        items = list()
        for row in c:
            if row[0] not in ignore:
                items.append(row)
            if len(items) >= maxResults:
                break
        c.close()
        return items

