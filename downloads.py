from PyQt4 import QtNetwork

downloadManager = None


class _DownloadManager(object):
    def __init__(self, action):
        self.action = action

    def downloadFile(self, file):
        pass

    def downloadSpeed(self):
        pass

    def downloadProgress(self):
        pass


def initialize(action):
    global downloadManager
    downloadManager = _DownloadManager(action)

