from PyQt4 import QtCore, QtGui
import functools

aliases = dict()
commands = dict()
categories = dict()

def registerCommand(command):
    global commands, categories, aliases
    if command.name() in commands:
        raise Exception("Command already exists with the key '%s'!" % key)
    commands[command.name()] = command
    aliases[command.name()] = command
    if command.category() is not None:
        if command.category() not in categories:
            categories[command.category()] = [command]
        else:
            categories[command.category()].append(command)
    for alias in command.aliases():
        aliases[alias] = command

def find(cmd):
    global aliases
    if cmd in aliases:
        return [aliases[cmd]]
    else:
        return [aliases[c] for c in aliases if c.startswith(cmd)]


class Command(object):
    class CommandHelper(object):
        _actionProperties = (
            'objectName',
            'autoRepeat',
            'checkable',
            'checked',
            'enabled',
            'font',
            'icon',
            'iconText',
            'iconVisibleInMenu',
            'menuRole',
            'shortcut',
            'shortcutContext',
            'statusTip',
            'text',
            'toolTip',
            'visible',
            'whatsThis',
        )
        def __init__(self, function, properties):
            self.function = function
            self.properties = properties
            self.__name__ = function.__name__
            self.__dict__.update(function.__dict__)
            self.__doc__ = function.__doc__
            self._actions = dict()
            self.obj = None

        def register(self):
            registerCommand(self)

        def __get__(self, obj, type=None):
            self.obj = obj
            return self

        def __call__(self, *args, **kwargs):
            if self.obj is None:
                raise Exception("self.obj not set in Command descriptor!")
            return self.function(self.obj, *args, **kwargs)

        def action(self, parent):
            if parent not in self._actions:
                self._actions[parent] = QtGui.QAction(parent)

                # Fill all QAction-related properties.
                for key, value in self.properties.iteritems():
                    if key in self._actionProperties:
                        self._actions[parent].setProperty(key, QtCore.QVariant(value))

                # Property fallbacks
                if 'objectName' not in self.properties:
                    if 'name' in self.properties:
                        self._actions[parent].setProperty('objectName', QtCore.QVariant(self.properties['name']))
                    else:
                        self._actions[parent].setProperty('objectName', QtCore.QVariant(self.__name__))
                if 'text' not in self.properties:
                    if 'longName' in self.properties:
                        self._actions[parent].setProperty('text', QtCore.QVariant(self.properties['longName']))
                    elif 'name' in self.properties:
                        self._actions[parent].setProperty('text', QtCore.QVariant(self.properties['name']))
                if 'toolTip' not in self.properties and 'description' in self.properties:
                    self._actions[parent].setProperty('toolTip', QtCore.QVariant(self.properties['description']))
                self._actions[parent].connect(self._actions[parent], QtCore.SIGNAL("triggered()"), self.__call__)
            return self._actions[parent]

        def name(self):
            return self.properties.get('name', self.__name__)

        def longName(self):
            return self.properties.get('longName', self.properties.get('text', self.properties.get('name', self.__name__)))

        def aliases(self):
            return self.properties.get('aliases', ())

        def description(self):
            return self.properties.get('description', self.properties.get('toolTip', None))

        def icon(self):
            return self.properties.get('icon', None)

        def category(self):
            return self.properties.get('category', None)

    def __init__(self, **kwargs):
        """ Creates a command from a method.

        Various keyword parameters are accepted. Here are the most common:
            name
            longName
            aliases
            description
            icon
            category

        Other possible parameters:
            objectName
            autoRepeat
            checkable
            checked
            enabled
            font
            iconText
            iconVisibleInMenu
            menuRole
            shortcut
            shortcutContext
            statusTip
            text
            toolTip
            visible
            whatsThis

        """
        self.kwargs = kwargs

    def __call__(self, function):
        self.function = function
        return Command.CommandHelper(function, self.kwargs)

