import sqlite3, os.path
from PyQt4 import QtCore, QtNetwork
from settings import *

class NetworkCookieJar(QtNetwork.QNetworkCookieJar):
    settingsPrefix = "sessions"
    settingsDef = [
        BoolVar("saveSessionCookies", True, name="Save cookies marked as session-only")
    ]

    def __init__(self, parent = None, session = "Default Session"):
        self.parent = parent
        self.session = session
        self.cookieDB = "cookies.db"
        QtNetwork.QNetworkCookieJar.__init__(self)

        Settings.createSettings(self.__class__,
			self.settingsPrefix,
			self.settingsDef,
			"Cookies",
            QtGui.QIcon(":icons/settings-cookies.png"))

    def loadCookiesFromDisk(self):
        if os.path.exists(self.cookieDB):
            def intToBool(val):
                if val == 0:
                    return False
                else:
                    return True

            conn = sqlite3.connect(self.cookieDB)
            c = conn.cursor()
            c.execute('select name, value, domain, expiration, httpOnly, secure, text from cookies where session=?', (self.session, ))
            cookies = list()
            for cdef in c:
                cookie = QtNetwork.QNetworkCookie(cdef[0], cdef[1])
                cookie.setDomain(cdef[2])
                cookie.setExpirationDate(QtCore.QDateTime.fromString(cdef[3], "yyyy-MM-dd-HH-mm-ss"))
                cookie.setHttpOnly(intToBool(cdef[4]))
                cookie.setSecure(intToBool(cdef[5]))
                cookie.setPath(cdef[6])
                cookies.append(cookie)
            c.close()
            conn.close()
            self.setAllCookies(cookies)
    
    def saveCookiesToDisk(self):
        # Get the settings path.
        # Write cookie
        dbExists = os.path.exists(self.cookieDB)
        conn = sqlite3.connect(self.cookieDB)
        c = conn.cursor()

        if dbExists:
            c.execute('delete from cookies')
        else:
            c.execute('create table cookies (session text, name blob, value blob, domain text, expiration text, httpOnly int, secure int, text path)')

        def boolToInt(val):
            if val:
                return 1
            else:
                return 0

        for cookie in self.allCookies():
            if not cookie.isSessionCookie() or self.settings["saveSessionCookies"]:
                c.execute('insert into cookies values(?, ?, ?, ?, ?, ?, ?, ?)', (
                    self.session,
                    bytes(cookie.name()),
                    bytes(cookie.value()),
                    str(cookie.domain()),
                    str(cookie.expirationDate().toString("yyyy-MM-dd-HH-mm-ss")),
                    boolToInt(cookie.isHttpOnly()),
                    boolToInt(cookie.isSecure()),
                    str(cookie.path())))
        conn.commit()
        c.close()
        conn.close()

cookieJar = None
networkAccessManager = None

def getSession():
    global cookieJar, networkAccessManager

    if cookieJar is None:
        print "Making new shiny NetworkCookieJar."
        cookieJar = NetworkCookieJar()
        cookieJar.loadCookiesFromDisk()

    if networkAccessManager is None:
        print "Making new QNetworkAccessManager."
        networkAccessManager = QtNetwork.QNetworkAccessManager()
        networkAccessManager.setCookieJar(cookieJar)

    return networkAccessManager

