#!/bin/bash
cd `dirname $0`
echo "<RCC>\n  <qresource prefix=\"icons\" >\n`ls -1 *.png | sed 's/.*/    <file>&<\/file>/'`\n  </qresource>\n</RCC>\n" | sed 's/\\n/\n/g' > mainbrowser.qrc
pyrcc4 -o ../mainbrowser_rc.py mainbrowser.qrc
