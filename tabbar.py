from PyQt4 import QtGui

class TabBar(QtGui.QTabBar):
    def __init__(self, parent=None):
        QtGui.QTabBar.__init__(self, parent)
        self.tabObjects = list()

    def addTab(self, icon, title, tab):
        idx = QtGui.QTabBar.addTab(self, icon, title)
        self.setTabObject(idx, tab)
        return idx

    def insertTab(self, index, icon, title, tab):
        idx = QtGui.QTabBar.insertTab(self, index, icon, title)
        self.setTabObject(idx, tab)
        return idx

    def tabObject(self, index):
        return self.tabObjects[index]

    def setTabObject(self, index, tab):
        self.tabObjects[index] = tab

    def indexOfTab(self, tab):
        if tab not in self.tabObjects:
            raise Exception("Tab not in tab bar?")

        return self.tabObjects.index(tab)

    def tabInserted(self, index):
        QtGui.QTabBar.tabInserted(self, index)
        self.tabObjects.insert(index, None)

    def tabRemoved(self, index):
        QtGui.QTabBar.tabRemoved(self, index)
        del self.tabObjects[index]
        if index == self.currentIndex():
            self.currentChanged.emit(index)

    def tabMoved(self, fromIndex, toIndex):
        QtGui.QTabBar.tabMoved(self, fromIndex, toIndex)
        tab = self.tabObject(fromIndex)
        self.tabRemoved(fromIndex)
        self.tabInserted(toIndex)
        self.setTabObject(toIndex, tab)

    def currentTab(self):
        if self.currentIndex() == -1 or self.currentIndex() >= len(self.tabObjects):
            return None
        return self.tabObjects[self.currentIndex()]

    def removeTabByObject(self, tab):
        if tab not in self.tabObjects:
            raise Exception("Tab not in tab bar?")

        self.removeTab(self.indexOfTab(tab))

    def setCurrentTab(self, tab):
        if tab not in self.tabObjects:
            raise Exception("Tab not in tab bar?")

        self.setCurrentIndex(self.indexOfTab(tab))

