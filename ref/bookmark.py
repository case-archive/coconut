#!/usr/bin/env python

import sqlite3

def connect():
        try:
            db = sqlite3.connect('bookmark.db')
            db.execute("CREATE TABLE IF NOT EXISTS bookmark(title text, url text)")
            db.commit()
        except IOError:
            print "cannot connect DB"    
            db = None
        return db
        
def read(db):
        booklist = []
        booklist = db.execute("select title,url from bookmark").fetchall()
        #print booklist
        return booklist

def add(db,data):
        #print data
        db.execute("INSERT INTO bookmark(title, url) VALUES(?,?)",data)
        db.commit()
        
def delete(db,data):
        if data.has_key('title') and data.has_key('url'):
            db.execute("delete from bookmark where title=:title and url=:url",data)
            db.commit()
        
def refresh(db):
         booklist = db.execute("select title,url from bookmark").fetchall()
         return booklist
    
def close(db):
        db.close()
        
     
