from PyQt4 import QtCore, QtGui
import requestfilters, history

model = None

class CompletionModel(QtCore.QAbstractTableModel):
    def __init__(self, parent):
        QtCore.QAbstractTableModel.__init__(self, parent)
        self.update()
        requestfilters.notifyOnUpdate(self)
        history.notifyOnUpdate(self)

    def update(self, tag=None, items=None):
        # We expect getCompletions() to return a list of 4-tuples:
        # (request, description, color, tooltip)
        if items is None:
            self.completions = list()
            for filter in requestfilters.filters:
                self.completions += filter.getCompletions()
        else:
            self.completions += items

        self.completions.sort(key=lambda x: x[0].lower())

    def flags(self, index):
        """Used by other components to obtain information about each item
        provided by the model. In many models, the combination of flags should
        include Qt::ItemIsEnabled and Qt::ItemIsSelectable.
        """
        return QtCore.Qt.ItemIsEnabled

    def data(self, index, role=QtCore.Qt.DisplayRole):
        """Used to supply item data to views and delegates. Generally, models
        only need to supply data for Qt::DisplayRole and any application-
        specific user roles, but it is also good practice to provide data for
        Qt::ToolTipRole, Qt::AccessibleTextRole, and Qt::AccessibleDescriptionRole.
        """
        if role == QtCore.Qt.DisplayRole or role == QtCore.Qt.EditRole:
            if self.completions[index.row()][index.column()] is None:
                return QtCore.QVariant()
            else:
                return QtCore.QVariant(self.completions[index.row()][index.column()])
        elif role == QtCore.Qt.ToolTipRole:
            if self.completions[index.row()][3] is None:
                return QtCore.QVariant()
            else:
                return QtCore.QVariant(self.completions[index.row()][3])
        elif role == QtCore.Qt.ForegroundRole:
            return QtCore.QVariant(QtGui.QBrush(self.completions[index.row()][2]))
        else:
            return QtCore.QVariant()

    def headerData(self, section, orientation, role=QtCore.Qt.DisplayRole):
        """Provides views with information to show in their headers. The
        information is only retrieved by views that can display header
        information.
        """
        return QtCore.QVariant()

    def rowCount(self, parent=QtCore.QModelIndex()):
        """Provides the number of rows of data exposed by the model.
        """
        if parent.isValid():
            return 0
        return len(self.completions)

    def columnCount(self, parent=QtCore.QModelIndex()):
        """Provides the number of columns of data exposed by the model. List
        models do not provide this function because it is already implemented
        in QAbstractListModel.
        """
        if parent.isValid():
            return 0
        return 2

def initialize(parent):
    global model
    model = CompletionModel(parent)
